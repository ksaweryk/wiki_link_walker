import requests
from bs4 import BeautifulSoup
import re

URL_RE = re.compile(r"(?P<protocol>\w+://)(?P<host>[\w.]+:?\d*)(?P<path>/[\w/]*)")


class RingBinder:
    def __init__(self, start, end, ttl, match=re.compile("^/wiki/[^:]*$")):
        self.base, path = split_url(start)
        self.goal = end
        self.re = match
        print(self.wiki_page(path, ttl))

    def wiki_page(self, path, ttl, before=()):
        if ttl <= 0:
            return
        elif self.base+path == self.goal:
            return before

        soup = BeautifulSoup(requests.get(self.base+path).text)
        content = soup.body.find("div", {"id": "content"})

        for tag in content.find_all(self.a_filter):
            if tag["href"] not in before and tag["href"] != path:
                result = self.wiki_page(tag["href"], ttl-1, before+(path,))
                if result is not None:
                    return result

    def a_filter(self, tag):
        if tag.name == "a" and self.re.match(tag.get("href", "")):
            return True

def split_url(url):
    match = URL_RE.match(url)
    if match is None:
        raise ValueError("Invalid url: '{}'.".format(url))
    return match.group("protocol")+match.group("host"), match.group("path")
